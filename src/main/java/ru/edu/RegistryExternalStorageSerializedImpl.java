package ru.edu;

import ru.edu.model.Registry;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

public class RegistryExternalStorageSerializedImpl
        implements RegistryExternalStorage {
    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public Object readFrom(final String filePath) {

        if (filePath == null) {
            throw new IllegalArgumentException("filePath equals null");
        }

        Registry registry;

        try (FileInputStream inputStream = new FileInputStream(filePath);
             ObjectInputStream objectInputStream
                     = new ObjectInputStream(inputStream)) {
                registry = (Registry) objectInputStream.readObject();
                return registry;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry registry) {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath equals null");
        }

        try (FileOutputStream inputStream = new FileOutputStream(filePath);
             ObjectOutputStream objectOutputStream
                     = new ObjectOutputStream(inputStream)) {

            objectOutputStream.writeObject(registry);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
