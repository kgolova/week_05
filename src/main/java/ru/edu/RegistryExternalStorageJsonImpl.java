package ru.edu;

import ru.edu.model.Registry;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RegistryExternalStorageJsonImpl
        implements RegistryExternalStorage {
    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public Object readFrom(final String filePath) {

        if (filePath == null) {
            throw new IllegalArgumentException("filePath equals null");
        }

        ObjectMapper mapper = new ObjectMapper();

        try {
            String json = streamToString(new FileInputStream(filePath));
            Registry registry = mapper.readValue(json, Registry.class);
            return registry;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry registry) {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath equals null");
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(filePath), registry);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Преобразовать поток в строку.
     *
     * @param inputStream путь
     * @return sb
     */
    private String streamToString(final InputStream inputStream)
            throws IOException {

        StringBuilder sb = new StringBuilder();

        String line;

        BufferedReader br =
                new BufferedReader(new InputStreamReader(inputStream));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        br.close();
        return sb.toString();

    }
}
