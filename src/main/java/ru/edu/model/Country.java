package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Country implements Serializable {

    private static final long serialVersionUID = 124L;

    /**
     * Название страны.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name")
    private String name;

    /**
     * Список исполнителей.
     */
    @JsonProperty()
    @JacksonXmlElementWrapper(localName = "Artist", useWrapping = false)
    @JacksonXmlProperty(localName = "Artist")
    private List<Artist> artists = new ArrayList<>();

    /**
     * Конструткор по-умолчанию.
     */
    public Country() {

    }

    /**
     * Конструткор.
     * @param nametmp
     */
    public Country(final String nametmp) {
        this.name = nametmp;
    }

    /**
     * Получить название страны.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Установить название страны.
     * @param nametmp
     */
    public void setName(final String nametmp) {
        this.name = nametmp;
    }

    /**
     * Список артистов.
     * @return artists
     */
    public List<Artist> getArtists() {
        return artists;
    }

    /**
     * Исполнитель.
     * @param artisttmp
     */
    public void addArtists(final Artist artisttmp) {

        this.artists.add(artisttmp);

    }

    /**
     * Установить список исполнителей.
     * @param artistsList
     */
    public void setArtists(final List<Artist> artistsList) {
        this.artists = artistsList;
    }

    /**
     * Строковое представление.
     * @return
     */
    @Override
    public String toString() {
        return "Country{"
                + "name='" + name + '\''
                + ", artists=" + artists
                + '}';
    }


}



