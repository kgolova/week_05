package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CD {

    /**
     * title.
     */
    @JacksonXmlProperty(localName = "TITLE")
    private String title;

    /**
     * artistName.
     */
    @JacksonXmlProperty(localName = "ARTIST")
    private String artistName;

    /**
     * country.
     */
    @JacksonXmlProperty(localName = "COUNTRY")
    private String country;

    /**
     * company.
     */
    @JacksonXmlProperty(localName = "COMPANY")
    private String company;

    /**
     * price.
     */
    @JacksonXmlProperty(localName = "PRICE")
    private double price;

    /**
     * year.
     */
    @JacksonXmlProperty(localName = "YEAR")
    private int year;

    /**
     * Получить заголовок.
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Установить заголовок.
     * @param titletmp
     */
    public void setTitle(final String titletmp) {
        this.title = titletmp;
    }

    /**
     * Получить Имя исполнителя.
     * @return artistName
     */
    public String getArtist() {
        return artistName;

    }

    /**
     * Установить Имя исполнителя.
     * @param nametmp
     */
    public void setArtist(final String nametmp) {
        this.artistName = nametmp;
    }


    /**
     * Получить страну.
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Установить установить страну.
     * @param countrytmp
     */
    public void setCountry(final String countrytmp) {
        this.country = countrytmp;
    }

    /**
     * Получить company.
     * @return company
     */
    public String getCompany() {
        return company;
    }


    /**
     * Получить companytmp.
     * @param companytmp
     */
    public void setCompany(final String companytmp) {
        this.company = companytmp;
    }

    /**
     * Получить цену.
     * @return price
     */
    public double getPrice() {
        return price;
    }

    /**
     * Установить цену.
     * @param pricetmp
     */
    public void setPrice(final double pricetmp) {
        this.price = pricetmp;
    }


    /**
     * Получить год альбома.
     * @return year
     */
    public int getYear() {
        return year;
    }

    /**
     * Установить год.
     * @param yeartmp
     */
    public void setYear(final int yeartmp) {
        this.year = yeartmp;
    }


    /**
     * Получить строковое представление.
     */
    @Override
    public String toString() {
        return "CD{"
                + "title='" + title + '\''
                + ", artist='" + artistName + '\''
                + ", country='" + country + '\''
                + ", company='" + company + '\''
                + ", price=" + price
                + ", year=" + year
                + '}';
    }
}


