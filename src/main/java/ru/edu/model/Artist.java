package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Artist implements Serializable {

    private static final long serialVersionUID = 126L;

    /**
     * Имя исполнителя.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    /**
     * Список альбомов.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "Album")
    private List<Album> albums = new ArrayList<>();

    /**
     * Конструтор.
     * @param nametmp
     */
    public Artist(final String nametmp) {
        this.name = nametmp;
    }


    /**
     * Конструтор по-умолчанию.
     */
    public Artist() {
    }

    /**
     * Получить имя исполнителя.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * устаноить список албюомов.
     * @param albumstmp
     */
    public void setAlbums(final List<Album> albumstmp) {
        this.albums = albumstmp;
    }
}
