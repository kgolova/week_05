package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

public class Catalog {

    /**
     * Список CD.
     */
    @JacksonXmlElementWrapper(localName = "CD", useWrapping = false)
    @JacksonXmlProperty(localName = "CD")
    @JsonProperty("CD")
    private List<CD> cdList = new ArrayList<>();


    /**
     * Конструтор.
     *
     */
    public Catalog() {

    }

    /**
     * Конструтор.
     * @param cdListtmp
     */
    public Catalog(final List<CD> cdListtmp) {
        this.cdList = cdListtmp;
    }

    /**
     * Получить Список CD.
     * @return cdList
     */
    public List<CD> getCdList() {
        return cdList;
    }

    /**
     * Строковое представление.
     * @return
     */
    @Override
    public String toString() {
        return "Catalog{ catalog="
                + cdList
                + '}';

    }

}




