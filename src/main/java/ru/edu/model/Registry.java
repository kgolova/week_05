package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@JacksonXmlRootElement(localName = "ArtistRegistry")
public class Registry implements Serializable {

    private static final long serialVersionUID = 125L;


    /**
     * Список стран.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("Country")
    private List<Country> countries = new ArrayList<>();

    /**
     * Получить список стран.
     * @return countries
     */
    public List<Country> getCountries() {
        return countries;
    }

    /**
     * Конструткор по-умолчанию.
     */
    public Registry() {

    }

    /**
     * Установить список стран.
     * @param countriestmp
     */
    public void setCountries(final List<Country> countriestmp) {
        this.countries = countriestmp;
    }

    /**
     * Добавить страну в список.
     * @param value
     */
    public void addCountries(final Country value) {
        countries.add(value);
    }

    /**
     * Строковое представление.
     * @return
     */
    @Override
    public String toString() {
        return "Registry{"
                + "countries=" + countries
                + '}';
    }
}
