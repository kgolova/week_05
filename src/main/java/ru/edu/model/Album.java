package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

public class Album implements Serializable {

    private static final long serialVersionUID = 123L;

    /**
     *  Название альбома.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    /**
     * Год альбома.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    private long year;

    /**
     * Конструктор.
     * @param nametmp
     * @param yeartmp
     */
    public Album(final String nametmp, final long yeartmp) {
        this.name = nametmp;
        this.year = yeartmp;
    }

    /**
     *  Конструктор по-умолчанию.
     */
    public Album() {

    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Получить год.
     * @return year
     */
    public long getYear() {
        return year;
    }

    /**
     * Получить имя.
     * @param nametmp
     */
    public void setName(final String nametmp) {
        this.name = nametmp;
    }

    /**
     * Установить год.
     * @param value
     */
    public void setYear(final long value) {
        this.year = value;
    }

    /**
     * Получить строковое представление.
     * @return
     */
    @Override
    public String toString() {
        return "Album{ name='" + name + '\''
                + ", year=" + year
                + '}';

    }

}

















