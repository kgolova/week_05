package ru.edu;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;
import ru.edu.model.Artist;
import ru.edu.model.Album;
import ru.edu.model.Country;
import ru.edu.model.CD;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.File;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.List;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.Set;
import java.util.Map;

public class RegistryExternalStorageXmlImpl implements RegistryExternalStorage {
    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public Object readFrom(final String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath equals null");
        }

        XmlMapper xmlMapper = new XmlMapper();
        try {
            String xml = streamToString(new FileInputStream(filePath));

            Catalog catalog = xmlMapper.readValue(xml, Catalog.class);

            return artistByCountry(catalog);

        } catch (IOException e) {

            e.printStackTrace();

        }

        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry registry) {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath equals null");
        }

        XmlMapper xmlMapper = new XmlMapper();

        try {
            xmlMapper.writeValue(new File(filePath), registry);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Преобразовать поток в строку.
     *
     * @param inputStream путь
     * @return StringBuilder
     */
    private String streamToString(final InputStream inputStream)
            throws IOException {

        StringBuilder sb = new StringBuilder();

        String line;

        BufferedReader br =
                new BufferedReader(new InputStreamReader(inputStream));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        br.close();
        return sb.toString();

    }


    private Registry artistByCountry(final Catalog catalog) {

        Map<String, List<Artist>> artists = new HashMap<>();
        Map<String, List<Album>> albums = new HashMap<>();
        List<Country> countries = new ArrayList<>();

        Set<String> countriesStr =
            catalog.getCdList().stream().
                   map(cd -> cd.getCountry()).
                    distinct().
                    collect(Collectors.toCollection(LinkedHashSet::new));

        for (CD cd : catalog.getCdList()) {
                if (!artists.containsKey(cd.getCountry())) {
                    artists.put(cd.getCountry(), new ArrayList<>());
                }
                artists.get(cd.getCountry()).add(new Artist(cd.getArtist()));
                if (!albums.containsKey(cd.getArtist())) {
                    albums.put(cd.getArtist(), new ArrayList<>());
                }
                albums.get(cd.getArtist()).add(
                    new Album(cd.getTitle(), cd.getYear())
                );
       }

        for (List<Artist> countryArtists : artists.values()) {
            for (Artist artist : countryArtists) {
                List<Album> albumstemp = albums.get(artist.getName());
                artist.setAlbums(albumstemp);
            }
        }

        for (String countryStr : countriesStr) {
            Country country = new Country(countryStr);
            country.setArtists(artists.get(countryStr));
            countries.add(country);
        }

        Registry registry = new Registry();
        registry.setCountries(countries);
        return registry;
    }
}

