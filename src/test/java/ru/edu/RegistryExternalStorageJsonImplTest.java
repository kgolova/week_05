package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.model.Registry;

import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;

public class RegistryExternalStorageJsonImplTest extends TestCase {

    RegistryExternalStorage jsonParser = new RegistryExternalStorageJsonImpl();
    RegistryExternalStorage xmlParser  = new RegistryExternalStorageXmlImpl();

    String outputFileJson = "output/artist_by_country.json";
    String outputFileJsonTest = "output/artist_by_country_test.json";
    String inputFileXml = "input/cd_catalog.xml";

    @Mock
    private Registry testRegistry;

    @Before
    public void setUp() {
        openMocks(this);
        testRegistry = mock(Registry.class);
    }

    @Test
    public void testReadFrom() {

        Registry registry = (Registry) xmlParser.readFrom(inputFileXml);
        jsonParser.writeTo(outputFileJson, registry);

        Registry registry1 = (Registry) jsonParser.readFrom(outputFileJsonTest);

        assertNotNull(registry1);

    }

    @Test
    public void testReadFromNull() {

        boolean success = false;
        try {
            jsonParser.readFrom(null);
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testWriteTo() {

        Registry registry = (Registry) xmlParser.readFrom(inputFileXml);
        jsonParser.writeTo(outputFileJson, registry);
        assertNotNull(outputFileJson);
    }

    @Test
    public void testWriteNull() {

        boolean success = false;
        try {
            jsonParser.writeTo(null, testRegistry);;
        } catch (IllegalArgumentException e) {
            success = true;
        }
    }
}
