package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.model.Registry;

import java.io.File;

import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;

public class RegistryExternalStorageSerializedImplTest extends TestCase {

    RegistryExternalStorage serialized = new RegistryExternalStorageSerializedImpl();
    RegistryExternalStorage xmlParser  = new RegistryExternalStorageXmlImpl();

    String inputFile = "input/cd_catalog.xml";
    String outputFile = "output/artist_by_country.serialized";

    @Mock
    private Registry testRegistry;

    Registry registry;

    @Before
    public void setUp() throws Exception {

        openMocks(this);
        testRegistry = mock(Registry.class);

        registry = (Registry) xmlParser.readFrom(inputFile);
    }

    @Test
    public void testWriteTo() {

        serialized.writeTo(outputFile, registry);

    }

    @Test
    public void testWriteToNull() {

        boolean success = false;
        try {
            serialized.writeTo(null, testRegistry );;
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testReadFrom() {
        Registry registry = (Registry) serialized.readFrom(outputFile);
        assertNotNull(registry);
    }

    @Test
    public void testReadFromNull() {

        boolean success = false;
        try {
            serialized.readFrom(null);;
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }


}