package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.model.Registry;

import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;

public class RegistryExternalStorageXmlImplTest extends TestCase {

    RegistryExternalStorage xmlParser  = new RegistryExternalStorageXmlImpl();

    String inputFileXml = "input/cd_catalog.xml";
    String outputFileXml = "output/artist_by_country.xml";

    @Mock
    private Registry testRegistry;

    @Before
    public void setUp() {
        openMocks(this);
        testRegistry = mock(Registry.class);
    }

    public void testReadFrom() {
        Registry registry = (Registry) xmlParser.readFrom(inputFileXml);
        xmlParser.writeTo(outputFileXml, registry);

    }

    @Test
    public void testReadFromNull() {

        boolean success = false;
        try {
            xmlParser.readFrom(null);;
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }

    public void testWriteTo() {

        Registry registry = (Registry) xmlParser.readFrom(inputFileXml);
        xmlParser.writeTo(outputFileXml, registry);
        assertNotNull(outputFileXml);
    }

    @Test
    public void testWriteNull() {

        boolean success = false;
        try {
            xmlParser.writeTo(null, testRegistry);;
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }
}

