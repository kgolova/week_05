package ru.edu.model;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;


public class CatalogTest extends TestCase {

    List<CD> cdList;

    @Mock
    private CD cd1;
    @Mock
    private CD cd2;

    @Before
    public void setUp() {
        openMocks(this);
        cd1 = mock(CD.class);
        cd2 = mock(CD.class);

        cdList = new ArrayList<>();
        cdList.add(cd1);
        cdList.add(cd2);

    }

    @Test
    public void testCatalog() {

        Catalog catalog1 = new Catalog();
        Catalog catalog2 = new Catalog(cdList);

    }

    @Test
    public void testGetCdList() {
        Catalog catalog2 = new Catalog(cdList);
        assertEquals(2, catalog2.getCdList().size());
    }


}