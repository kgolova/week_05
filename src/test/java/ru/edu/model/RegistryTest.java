package ru.edu.model;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;


public class RegistryTest extends TestCase {

    @Mock
    private Country country1;

    @Mock
    private Country country2;

    List<Country> countries = new ArrayList<>();
    Registry registry = new Registry();

    @Before
    public void setUp() {
        openMocks(this);
        country1 = mock(Country.class);
        country2 = mock(Country.class);
    }

    @Test
    public void testGetCountries() {

        countries.add(country1);
        countries.add(country2);

        registry.setCountries(countries);

        assertEquals(2, registry.getCountries().size());

    }

    @Test
    public void testSetCountries() {

        countries.add(country1);
        countries.add(country2);

        registry.setCountries(countries);

        assertEquals(2, registry.getCountries().size());

    }

    @Test
    public void testAddCountries() {

        registry.addCountries(country1);
        assertEquals(1, registry.getCountries().size());
    }


}