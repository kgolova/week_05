package ru.edu.model;

import junit.framework.TestCase;

public class AlbumTest extends TestCase {

    Album album1  = new Album("test1",2020);

    public void testTestGetName() {
    }

    public void testGetYear() {
        assertEquals(2020, album1.getYear());

    }

    public void testTestSetName() {
        Album album2 = new Album();
        album2.setName("test2");
        assertEquals("test2", album2.getName());
    }

    public void testSetYear() {
        Album album2 = new Album();
        album2.setName("test2");
        album2.setYear(2020);
        assertEquals(2020, album2.getYear());
    }

    public void testTestToString() {
        assertEquals("Album{ name='test1', year=2020}", album1.toString());
    }
}