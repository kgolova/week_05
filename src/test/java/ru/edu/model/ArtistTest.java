package ru.edu.model;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;

public class ArtistTest extends TestCase {

    Artist artist1 = new Artist("Ozzy Osbourne");
    Artist artist2 = new Artist();

    @Mock
    private Album album1;

    @Mock
    private Album album2;

    @Before
    public void setUp() {
        openMocks(this);
        album1 = mock(Album.class);
        album2 = mock(Album.class);
    }

    @Test
    public void testTestGetName() {

        assertEquals("Ozzy Osbourne", artist1.getName());
    }

    @Test
    public void testSetAlbums() {
        List<Album> albums= new ArrayList<>();
        albums.add(album1);
        albums.add(album2);

        artist1.setAlbums(albums);
    }
}