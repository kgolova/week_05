package ru.edu.model;

import junit.framework.TestCase;
import org.junit.Before;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;

public class CountryTest extends TestCase {

    Country country = new Country("USA");

    @Mock
    private Artist artist1;

    @Mock
    private Artist artist2;

    List<Artist>  artistList = new ArrayList<>();

    @Before
    public void setUp() {
        openMocks(this);
        artist1 = mock(Artist.class);
        artist2 = mock(Artist.class);
    }

    public void testTestGetName() {
        assertEquals("USA", country.getName());
    }

    public void testTestSetName() {
        country.setName("UK");
        assertEquals("UK", country.getName());
    }

    public void testGetArtists() {

        artistList.add(artist1);
        country.setArtists(artistList);

        assertEquals(1, country.getArtists().size());
    }

    public void testAddArtists() {

        country.addArtists(artist1);
        assertEquals(1, country.getArtists().size());
    }

    public void testSetArtists() {
        artistList.add(artist1);
        artistList.add(artist2);
        country.setArtists(artistList);

        assertEquals(2, country.getArtists().size());
    }

    public void testTestToString() {
        assertEquals("Country{name='USA', artists=[]}", country.toString());
    }
}