package ru.edu.model;

import junit.framework.TestCase;
import org.junit.Before;

public class CDTest extends TestCase {

    CD testCD;
    CD cd;

    @Before
    public void setUp() throws Exception {

        testCD = new CD();
        testCD.setArtist("Test Test");
        testCD.setCompany("TEST");
        testCD.setCountry("USA");
        testCD.setPrice(30);
        testCD.setTitle("TEST");
        testCD.setYear(2022);

    }

    public void testGetTitle() {
        assertEquals("TEST", testCD.getTitle());
    }

    public void testSetTitle() {
        cd = new CD();
        cd.setTitle("TEST1");
        assertEquals("TEST1", cd.getTitle());
    }

    public void testGetArtist() {
        assertEquals("Test Test", testCD.getArtist());
    }

    public void testSetArtist() {
        cd = new CD();
        cd.setArtist("Test Test");
        assertEquals("Test Test", cd.getArtist());
    }

    public void testGetCountry() {
        assertEquals("USA", testCD.getCountry());
    }

    public void testSetCountry() {
        cd = new CD();
        cd.setCountry("USA");
        assertEquals("USA", cd.getCountry());
    }

    public void testGetCompany() {
        assertEquals("TEST", testCD.getCompany());
    }

    public void testSetCompany() {
        cd = new CD();
        cd.setCompany("TEST");
        assertEquals("TEST", cd.getCompany());
    }

    public void testGetPrice() {
        assertEquals(30.0, testCD.getPrice());
    }

    public void testSetPrice() {
        cd = new CD();
        cd.setPrice(50.0);
        assertEquals(50.0, cd.getPrice());
    }

    public void testGetYear() {
        assertEquals(2022, testCD.getYear());
    }

    public void testSetYear() {
        cd = new CD();
        cd.setYear(2021);
        assertEquals(2021, cd.getYear());
    }

    public void testTestToString() {
        assertEquals("CD{title='TEST', artist='Test Test', country='USA', company='TEST', price=30.0, year=2022}", testCD.toString());
    }
}